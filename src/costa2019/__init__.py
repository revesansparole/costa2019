"""
Data from canopy and soil thermal patterns in vineyards
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}
# {# pkglts, glabdata, after version
from .info import *
# #}

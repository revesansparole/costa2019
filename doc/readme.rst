Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://b326.gitlab.io/costa2019/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/costa2019/1.2.0/


.. image:: https://b326.gitlab.io/costa2019/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/costa2019


.. image:: https://b326.gitlab.io/costa2019/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/costa2019/


.. image:: https://badge.fury.io/py/costa2019.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/costa2019




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/costa2019/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/costa2019/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/costa2019/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/costa2019/commits/main

prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/b326/costa2019/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/b326/costa2019/commits/prod

.. |prod_coverage| image:: https://gitlab.com/b326/costa2019/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/b326/costa2019/commits/prod
.. #}

Data from canopy and soil thermal patterns in vineyards

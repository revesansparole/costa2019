"""
Fig1 (weather)
==============

Plot weather data to compare with fig1
"""
import matplotlib.pyplot as plt
import pandas as pd
from costa2019 import pth_clean

# read data
df = pd.read_csv(pth_clean / "fig1.csv", sep=";", comment="#", parse_dates=['date'], index_col=['date'])

# plot data
per_left = (pd.Period("2014-07-24", '1D'),
            pd.Period("2014-08-19", '1D'))
per_right = (pd.Period("2015-05-12", '1D'),
             pd.Period("2015-06-03", '1D'),
             pd.Period("2015-07-08", '1D'),
             pd.Period("2015-07-09", '1D'),
             pd.Period("2015-08-13", '1D'),
             pd.Period("2015-08-14", '1D'))

fig, axes = plt.subplots(3, 2, sharex='all', sharey='row', figsize=(10, 8), squeeze=False)

for pers, ax in zip([per_left, per_right], axes[0, :]):
    for per in pers:
        rs = df.loc[per.start_time:per.end_time, 'rs']
        ax.plot(rs.index.hour, rs, label=f"{per.start_time.date().isoformat()}")

    ax.legend(loc='upper left')
    ax.set_ylim(0, 1200)
    ax.set_ylabel("Rs [W.m-2]")

for pers, ax in zip([per_left, per_right], axes[1, :]):
    for per in pers:
        tc = df.loc[per.start_time:per.end_time, 'tc']
        ax.plot(tc.index.hour, tc, label=f"{per.start_time.date().isoformat()}")

    ax.set_ylim(0, 45)
    ax.set_ylabel("T [°C]")

for pers, ax in zip([per_left, per_right], axes[2, :]):
    for per in pers:
        rh = df.loc[per.start_time:per.end_time, 'rh']
        ax.plot(rh.index.hour, rh, label=f"{per.start_time.date().isoformat()}")

    ax.set_ylim(0, 100)
    ax.set_ylabel("rH [%]")

axes[0, 0].set_title("2014")
axes[0, 1].set_title("2015")

for ax in axes[-1, :]:
    ax.set_xticks(range(0, 25, 2))

fig.tight_layout()
plt.show()

"""
Table 4
=======

Spatial temperatures throughout the day
"""
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd

from costa2019 import pth_clean

# read data
df = pd.read_csv(pth_clean / "tab4.csv", sep=";", comment="#", parse_dates=['date'],
                 index_col=['date'])

# plot
fig, axes = plt.subplots(3, 1, figsize=(12, 7), squeeze=False)

for i, vname in enumerate(['tc_up', 'tc_clus', 'ts']):
    ax = axes[i, 0]
    for (variety, modality), sdf in df.groupby(['variety', 'modality']):
        crv, = ax.plot(sdf.index, sdf[f'{vname}_sun'], label=f"{variety} / {modality} sun")
        ax.plot(sdf.index, sdf[f'{vname}_shd'], '--', color=crv.get_color(), label=f"{variety} / {modality} shade")

    ax.legend(loc='upper left', ncol=2)
    ax.set_ylabel(f"{vname} [°C]")

for ax in axes[-1, :]:
    ax.set_xlabel("time of day [h]")
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

fig.tight_layout()
plt.show()

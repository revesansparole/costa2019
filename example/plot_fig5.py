"""
Fig5 (rad + temp)
=================

Plot canopy temp and radiation data to compare with fig5
"""
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from costa2019 import pth_clean

# read data
df = pd.read_csv(pth_clean / "fig5.csv", sep=";", comment="#", parse_dates=['date'], index_col=['date'])

# plot data
mods = ('ARA-DI1', 'ARA-DI2', 'TOU-DI1', 'TOU-DI2')

fig, axes = plt.subplots(1, 1, figsize=(10, 8), squeeze=False)
ax = axes[0, 0]
ax.set_title("2015-07-08 -> 2015-07-09")

for mod in mods:
    ax.plot(df.index, df[mod], label=mod)

ax.plot(df.index, df['TS'], '--', label="TS")
ax.plot(df.index, df['Tair'], lw=3, label="Tair")

ax.legend(loc='upper left')
ax.set_ylim(10, 65)
ax.set_ylabel("T [°C]")
ax.xaxis.set_major_formatter(mdates.DateFormatter("%H"))

ax = ax.twinx()
ax.plot(df.index, df['rs'], '--', label="rs")

ax.legend(loc='upper right')
ax.set_ylim(0, 1200)
ax.set_ylabel("solar radiation [W.m-2]")
ax.xaxis.set_major_formatter(mdates.DateFormatter("%H"))



# for ax in axes[-1, :]:
#     ax.set_xticks(range(0, 25, 2))

fig.tight_layout()
plt.show()
